import fs from "fs/promises";
import _ from "lodash";
import path from "path";
import { Promise as id3 } from "node-id3";
import { FRAME_IDENTIFIERS } from "node-id3/src/ID3Definitions";

let nAlbums = 0;
let nSongs = 0;
let nErrors = 0;

const albumDirs = [
  "/ceah/muzică/reordonate/albume-străine/",
  "/ceah/muzică/reordonate/albume-românești/",
];

const wantedTags = [
  "title",
  "artist",
  "album",
  "trackNumber",
  "year",
  "image",
  "genre",
];

const wantedTagCodes = wantedTags.map(
  (x) => FRAME_IDENTIFIERS.v4[x] || FRAME_IDENTIFIERS.v3[x],
);

function logError(error): void {
  process.stderr.write(JSON.stringify(error) + "\n");
  nErrors++;
}

class AlbumDir {
  path: string;
  bandName?: string;
  albumYear?: number;
  albumTitle?: string;

  constructor(path: string) {
    this.path = path;
  }

  async verify(): Promise<void> {
    const name = path.basename(this.path);

    const match = name.match(/^(.+?) - (\d{4}) - (.+)$/);

    if (match) {
      this.bandName = match[1];
      this.albumYear = parseInt(match[2], 10);
      this.albumTitle = match[2];
      if (this.albumYear < 1000 || this.albumYear > 2100) {
        logError({ path: this.path, text: "Album year looks invalid." });
      }
    } else {
      logError({ path: this.path, text: "Album dir name is invalid." });
    }

    if (!(await isDirectory(this.path))) {
      logError({ path: this.path, text: "Album path is not a dir." });
      return;
    }

    const files = await fs.readdir(this.path);

    // TODO: Validate cover.jpg.

    if (!files.includes("cover.jpg")) {
      logError({ path: this.path, text: "Album lacks cover.jpg." });
    }

    await Promise.all(
      files
        .filter((x) => x !== "cover.jpg")
        .map(async (file) => {
          const song = new Song(path.join(this.path, file));
          await song.verify();
          nSongs++;
        }),
    );

    // TODO: Verify that no songs are missing.
  }
}

class Song {
  path: string;
  order?: number;
  title?: string;

  constructor(path: string) {
    this.path = path;
  }

  async verify(): Promise<void> {
    const name = path.basename(this.path);

    const match = name.match(/^(\d{2})\. (.+?)\.mp3$/);

    if (match) {
      this.order = parseInt(match[1], 10);
      this.title = match[2];
    } else {
      logError({ path: this.path, text: "Song name is invalid." });
    }

    if (await isDirectory(this.path)) {
      logError({ path: this.path, text: "Expected this to be a file." });
      return;
    }

    const tags = await id3.read(this.path);
    if (this.title !== tags.title) {
      logError({
        path: this.path,
        text: "Title differs between ID3 and path.",
        id3Version: tags.title,
        pathVersion: this.title,
      });
    }

    const unwantedTags = _.difference(Object.keys(tags), [
      ...wantedTags,
      "raw",
    ]);
    const unwantedTagCodes = _.difference(
      Object.keys(tags.raw as Object),
      wantedTagCodes,
    );
    if (unwantedTags.length || unwantedTagCodes.length) {
      logError({
        path: this.path,
        text: "Unwanted tags",
        unwantedTags,
        unwantedTagCodes,
      });
    }
  }
}

async function isDirectory(path: string): Promise<boolean> {
  try {
    const stats = await fs.stat(path);
    return stats.isDirectory();
  } catch (error) {
    console.error("Error accessing the path:", error);
    return false;
  }
}

async function main() {
  await Promise.all(
    albumDirs.map(async (dir) => {
      const files = await fs.readdir(dir);
      await Promise.all(
        files.map(async (file) => {
          const albumDir = new AlbumDir(path.join(dir, file));
          await albumDir.verify();
          nAlbums++;
        }),
      );
    }),
  );

  console.log("Albums:", nAlbums);
  console.log("Songs:", nSongs);
  console.log("Errors:", nErrors);
}

main();
